package com.grmg.tuniweiser;

public class Room {
	public final String name;
	public final String address;
	public final String location;
	
	public Room(String name, String address, String location) {
		this.name = name;
		this.address = address;
		this.location = location;
	}
}
