package com.grmg.tuniweiser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

public class MainActivity extends Activity {

	// List view
	private ListView lv;

	// Listview Adapter
	ArrayAdapter<String> adapter;

	// Search EditText
	EditText inputSearch;

	// ArrayList for Listview
	ArrayList<HashMap<String, String>> productList;

	private JSONArray getRooms() {
		JSONArray jsonArray = null;
		try {
			StringBuilder builder = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					this.getResources().openRawResource(R.raw.rooms)));
			String line;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
			jsonArray = new JSONArray(builder.toString());
			return jsonArray;
		} catch (Exception e) {
			Log.i(MainActivity.class.getName(), "Exception while parsing JSON");
		}
		return jsonArray;
	}

	private void showRoomPopup(String name, String address, String location) {

		AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
		helpBuilder.setTitle(name);
		helpBuilder.setMessage(location + "; " + address);
		
		helpBuilder.setPositiveButton("Close",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		AlertDialog helpDialog = helpBuilder.create();
		helpDialog.show();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		JSONArray rooms = getRooms();
		List<String> roomNames = getRoomNames(rooms);
		final Map<String, Room> nameToRoom = getNameToRoom(rooms);

		lv = (ListView) findViewById(R.id.list_view);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				String roomName = lv.getAdapter().getItem(arg2).toString();
				Room room = nameToRoom.get(roomName);
				String address = room.address;
				String location = room.location;
				
				showRoomPopup(roomName, address, location);

			}
		});
		inputSearch = (EditText) findViewById(R.id.inputSearch);

		// Adding items to listview
		adapter = new ArrayAdapter<String>(this, R.layout.list_item,
				R.id.product_name, roomNames);
		lv.setAdapter(adapter);

		inputSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2,
					int arg3) {
				// When user changed the Text
				MainActivity.this.adapter.getFilter().filter(cs);
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}

			@Override
			public void afterTextChanged(Editable arg0) {
			}
		});

	}

	private Map<String, Room> getNameToRoom(JSONArray rooms) {
		Map<String, Room> nameToRoom = new HashMap<String, Room>();
		try {
			for (int i = 0; i < rooms.length(); i++) {
				JSONObject jsonObject = rooms.getJSONObject(i);
				String name = jsonObject.getString("room-name");
				String address = jsonObject.getString("address");
				String location = jsonObject.getString("location");
				nameToRoom.put(name, new Room(name, address, location));
			}
		} catch (JSONException e) {
			Log.i(MainActivity.class.getName(), "Exception while parsing JSON");
		}
		return nameToRoom;
	}

	private List<String> getRoomNames(JSONArray rooms) {
		List<String> roomNames = new ArrayList<String>();
		try {
			for (int i = 0; i < rooms.length(); i++) {
				JSONObject jsonObject = rooms.getJSONObject(i);
				roomNames.add(jsonObject.getString("room-name"));
			}
		} catch (JSONException e) {
			Log.i(MainActivity.class.getName(), "Exception while parsing JSON");
		}
		Collections.sort(roomNames);
		return roomNames;
	}

}
